class Testscript < Formula
  desc ""
  homepage ""
  url "https://gitlab.com/SamChb72/testscript/-/archive/v1.0.0/testscript-v1.0.0.tar.gz"
  version "1.0.0"
  sha256 "e08f4f2df7c9f1254c43394ca4043343d5aa603bf37d5acfadff4c97d569ce29"
  license ""

  def install
    bin.install "testscript"
  end

end
